import React, { useEffect, useState } from 'react'
import axios from 'axios';
import background from '../../assets/background.jpg'
import { Link, useNavigate } from 'react-router-dom';




export default function Registration() {

    let [user, setUser] = useState({ email: "", password: "", cpassword:"", name: "",})
    let [validationErrors, setErrors] = useState({name:'', email:'', password:'', cpassword:''})
    let [loading, setLoading] = useState(false)
    let navigate = useNavigate()

    useEffect(() => {
        document.body.style.overflowX = "hidden"
        document.title = "Register | Dummy App"

      }, [])


    async function register(event){

        event.preventDefault()
        setLoading(true)

        try {
            let response = await axios.post(`${process.env.REACT_APP_BaseURL}/users/register`, user)
            navigate("/login")
            setTimeout(() => setLoading(false), 300);

        } catch (error) {
            let newErrors = {name:'', email:'', password:'', cpassword:''}
            let errors = error.response.data.errors
            errors.forEach((error) => {
                if(error.includes('name')){newErrors.name = error.replace(/"/g, '')}
                if(error.includes('password') && !error.includes('cpassword')){newErrors.password = error.replace(/"/g, '')}
                if(error.includes('email') || error.includes('Email') ){newErrors.email = error.replace(/"/g, '').replace("E", "e")}
                if(error.includes('cpassword')){newErrors.cpassword = 'password confirmation does not match'}
            })
            setErrors(newErrors)
            setTimeout(() => setLoading(false), 300);
            
            // console.log(newErrors)
        }

    
    } 

    function handleChange(event) {
        let newUser = {...user}
        newUser[event.target.name] =event.target.value
        setUser(newUser)
        // console.log(newUser)
    }

    return (
        <div className="row">
            <div className="col-md-6 col-2" style={{backgroundImage: `url(${background})`,  height:"100vh", backgroundSize:"cover"}}>

            </div>
            <div className="col-md-6 col-10" style={{padding:"50px"}}>
                <div className="d-flex flex-row justify-content-center" >
                    <form className="w-75">
                        <div className="form-group mb-5">
                            <label>Name</label>
                            <input onChange={handleChange} type="email" className="form-control" name="name" placeholder="Enter name" />
                            <span style={{color:"red", fontSize:"12px"}}>{validationErrors.name}</span>
                        </div>
                        <div className="form-group mb-5">
                            <label>Email address</label>
                            <input onChange={handleChange} type="email" className="form-control" name="email" placeholder="Enter email" />
                            <span style={{color:"red", fontSize:"12px"}}>{validationErrors.email}</span>
                        </div>
                        <div className="form-group mb-5">
                            <label>Password</label>
                            <input onChange={handleChange} type="password" className="form-control" name="password" placeholder="Password" />
                            <span style={{color:"red", fontSize:"12px"}}>{validationErrors.password}</span>
                        </div>
                        <div className="form-group mb-5">
                            <label>Confirm Password</label>
                            <input onChange={handleChange} type="password" className="form-control" name="cpassword" placeholder="Password" />
                            <span style={{color:"red", fontSize:"12px"}}>{validationErrors.cpassword}</span>
                        </div>

                        <div className="d-flex flex-column justify-content-between">
                            <button onClick={register} type="submit" className="btn btn-primary mb-4">Create Account {loading ? <i className="fas fa-spinner fa-pulse"></i> : ''} </button>
                            <Link to='/login'>Already have an account?</Link>
                        </div>

                    </form>
                </div>
            </div>
        </div>


    )
}
