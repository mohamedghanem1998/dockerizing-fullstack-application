import React, { useEffect, useState } from 'react'
import axios from 'axios';
import background from '../../assets/background.jpg'
import { Link, useNavigate } from 'react-router-dom';


export default function Login(props) {

    let [user, setUser] = useState({ email: "", password: ""})
    let [validationError, setError] = useState('')
    let navigate = useNavigate()

    useEffect(() => {
        document.body.style.overflowX = "hidden"
        document.title = "Login | Dummy App"
      }, [])


    async function signIn(event){

        event.preventDefault()

        try {
            let response = await axios.post(`${process.env.REACT_APP_BaseURL}/users/login`, user)
            localStorage.setItem('token', response.data.token)
            navigate("/home")

        } catch (error) {
            let newError = 'Email and/or Password are Invalid*'
            setError(newError)           
        }
    } 

    function handleChange(event) {
        let newUser = {...user}
        newUser[event.target.name] =event.target.value
        setUser(newUser)
        // console.log(newUser)
    }

    return (
        <div className="row">
            <div className="col-md-6 col-2" style={{backgroundImage: `url(${background})`,  height:"100vh", backgroundSize:"cover"}}>

            </div>
            <div className="col-md-6 col-10" style={{padding:"50px"}}>

                {validationError ? <div className="mb-5" style={{color:"red", backgroundColor:"pink", padding:"7px", fontSize:"16px", width:"100%", border: "1px solid", textAlign:"center"}}>{validationError}</div> : ''}
                

                <div className="d-flex flex-row justify-content-center" >
                    <form className="w-75">

                        <div className="form-group mb-5">
                            <label>Email address</label>
                            <input onChange={handleChange} type="email" className="form-control" name="email" placeholder="Enter email" />
                        </div>
                        <div className="form-group mb-5">
                            <label>Password</label>
                            <input onChange={handleChange} type="password" className="form-control" name="password" placeholder="Password" />
                        </div>

                        <div className="d-flex flex-column justify-content-between">
                            <button onClick={signIn} type="submit" className="btn btn-primary mb-4">Log In </button>
                            <Link to='/register'>No account?</Link>
                        </div>

                    </form>
                </div>
            </div>
        </div>


    )
}
