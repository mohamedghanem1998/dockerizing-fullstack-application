import React, { Component } from 'react'

export default class NotFound extends Component {
    render() {
        return (
            <div style={{ marginTop: "45vh", textAlign: "center", fontSize: "45px", fontWeight: "bold", letterSpacing: "4px" }}>
                404 Not Found
            </div>
        )
    }
}
