import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Outlet } from 'react-router-dom';
import NavBar from '../NavBar/NavBar';
import Post from '../Post/Post';




export default function Home() {

  let [posts, setPosts] = useState([])

  useEffect(() => {
    document.title = "Home | Dummy App"
    getPosts()
  }, [])

  async function getPosts() {
    axios.get(`${process.env.REACT_APP_BaseURL}/posts/index`, {headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }})
    .then((response) => {
      setPosts(response.data.posts)
    }).catch((error) => {
      alert(`Something went wrong: ${error}`)
    })
  }


  return (
    <>
      <NavBar />


      <div className="container">
        <div className="row">
            {posts.map((post) => <Post key={post._id} details={post}/>)}
        </div>
      </div>


    </>
  )

}
