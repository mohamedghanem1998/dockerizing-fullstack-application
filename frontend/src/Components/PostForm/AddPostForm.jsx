import axios from 'axios'
import jwtDecode from 'jwt-decode'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import NavBar from '../NavBar/NavBar'


export default function PostForm() {

    let [post, setPost] = useState({ title: "", description: "", price: "", createdBy: jwtDecode(localStorage.getItem('token')).id })
    let [validationErrors, setErrors] = useState({title:false, description:false, price:false})
    let navigate = useNavigate()

    useEffect(() => {
        document.title = "Add Post | Dummy App"
    }, [])


    function handleChange(event) {
        let newPost = { ...post }
        newPost[event.target.name] = event.target.value
        setPost(newPost)
        console.log(newPost)
    }


    async function savePost(event) {

        event.preventDefault()

        try {
            await axios.post(`${process.env.REACT_APP_BaseURL}/posts/create`, post, { headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } })
            navigate("/home")

        } catch (error) {
            let flags = {}
            error.response.data.errors.forEach((err) => {
                if(err.includes('title')){flags["title"] = true}
                if(err.includes('description')){flags["description"] = true}
                if(err.includes('price')){flags["price"] = true}
            })

            setErrors(flags)
        }

    }

    return (
        <>
            <NavBar></NavBar>
            <div className="container">
                <div style={{ marginLeft: "auto", marginRight: "auto" }} className="w-75 my-5">
                    <form>
                        <div className="form-group mb-4">
                            <h5 style={{fontStyle:"italic"}}>Title</h5>
                            <input onChange={handleChange} className="w-50 form-control" name="title" />
                            <span style={{color:"red", fontSize:"10px"}}>{validationErrors["title"] ? 'title required*' : ''}</span>
                        </div>
                        <div className="form-group mb-4">
                            <h5 style={{fontStyle:"italic"}}>Description</h5>
                            <textarea onChange={handleChange} name="description" rows="4" className="form-control" />
                            <span style={{color:"red", fontSize:"10px"}}>{validationErrors["description"] ? 'description required*' : ''}</span>

                        </div>

                        <div className="form-group mb-4">
                            <h5 style={{fontStyle:"italic"}}>Price</h5>
                            <input onChange={handleChange} className="w-25 form-control" name="price" />
                            <span style={{color:"red", fontSize:"10px"}}>{validationErrors["price"] ? 'integer price required*' : ''}</span>

                        </div>

                        <button onClick={savePost} type="submit" className="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </>

    )
}
