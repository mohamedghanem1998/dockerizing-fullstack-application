import jwtDecode from 'jwt-decode';
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import AddPostForm from './AddPostForm';



export default function PostFormMiddleware(props) {

    const token = localStorage.getItem('token')
    let navigate = useNavigate()
    let [ok, setOk] = useState(false)

    useEffect(() => {
        if (!token) {
            navigate("/login")
        }

        else if (token) {
            try {
                jwtDecode(token)
                setOk(true)
            } catch (error) {
                alert("Please do not manipulate the token. Log in again to use the website")
                navigate("/login")
            }
        }

        else {
            setOk(true)
        }
    }, [])



    return (
        <>
            {ok ? <AddPostForm /> : ''}
        </>
    )
}
