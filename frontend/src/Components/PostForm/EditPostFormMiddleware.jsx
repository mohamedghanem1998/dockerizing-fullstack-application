import axios from 'axios';
import jwtDecode from 'jwt-decode';
import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom';
import NotFound from '../NotFound/NotFound';
import EditPostForm from './EditPostForm';



export default function EditFormMiddleware(props) {

    const token = localStorage.getItem('token')
    let navigate = useNavigate()
    let [ok, setOk] = useState(false)
    let [post_id, setID] = useState(useParams().id)


    async function checkPostOwner(){
        const response = await axios.get(`${process.env.REACT_APP_BaseURL}/posts/show/${post_id}`, { headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } })
        
        if(response.data.post.createdBy != jwtDecode(localStorage.getItem('token')).id){
            navigate("/404")
        }
    }


    useEffect(() => {
        if (!token) {
            navigate("/login")
        }

        else if (token) {
            try {
                jwtDecode(token)
                checkPostOwner()
                setOk(true)
            } catch (error) {
                alert("Please do not manipulate the token. Log in again to use the website")
                navigate("/login")
            }
        }

        else {
            setOk(true)
        }
    }, [])



    return (
        <>
            {ok ? <EditPostForm /> : ''}
        </>
    )
}
