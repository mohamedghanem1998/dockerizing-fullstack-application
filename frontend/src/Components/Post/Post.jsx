import axios from 'axios'
import jwtDecode from 'jwt-decode'
import React from 'react'
import { Link } from 'react-router-dom'


export default function Post(props) {


  async function destroy(id){
    try {
      await axios.delete(`${process.env.REACT_APP_BaseURL}/posts/delete/${id}`,  {headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }})
      window.location.reload();
      
    } catch (error) {
      console.log(error)
    }
  }


  return (
    <div  className="col-md-3 col-12 mt-5">
      <div className="card" style={{ width: "18rem" }}>
        <div className="card-body">
          <h5 className="card-title" style={{fontSize: "25px"}}>{props.details.title}</h5>
          <p className="card-text">{props.details.description}</p>
        </div>
        <ul className="list-group list-group-flush">
          <li className="list-group-item">Price: {props.details.price}</li>
          <li className="list-group-item">Created By: <br /> <span style={{color: 'grey'}}>{props.details.createdBy.name}</span> </li>
        </ul>
        {
        jwtDecode(localStorage.getItem('token')).id != props.details.createdBy._id ? '' :
          <div className="card-body">
            <Link to="#" onClick={()=>destroy(props.details._id)} className="card-link"><i className="fa fa-trash" style={{color: 'grey'}}aria-hidden="true"></i></Link>
            <Link to={`/editpost/${props.details._id}`} className="card-link"><i className="fas fa-edit"></i></Link>
          </div>
        }

    </div>  
    </div>
  )
}
