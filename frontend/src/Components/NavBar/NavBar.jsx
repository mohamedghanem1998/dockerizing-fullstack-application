import jwtDecode from 'jwt-decode'
import React, { useEffect, useState } from 'react'
import {Link, useNavigate } from 'react-router-dom'
import style from "./NavBar.module.css"


export default function NavBar() {

    let [name, setName] = useState('')
    const navigate = useNavigate()

    useEffect(() => {
        const token = localStorage.getItem('token')
        setName(jwtDecode(token).name)
    }, [])

    function logout(){
        localStorage.removeItem('token')
        navigate("/login")
    }

  return (
    <div className="container-fluid bg-dark p-4 " style={{color: 'white'}}>

        <div className={style.box1}>
            <div className={style.inner1}>
                <Link to="/home" className={[style.username, `me-5`].join(' ')}  style={{color: '#4a86ff', fontWeight:"bold"}}>{name}</Link>
                <Link to="/addpost" style={{color: 'white'}}className={style.add}>Add Post</Link>                
            </div>

            <div className={style.inner2}>
                <div><button onClick={logout} className="btn btn-danger">Log Out</button></div>
            </div>
        </div>

    </div>
  )
}
