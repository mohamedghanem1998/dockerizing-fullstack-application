import React, { useState } from 'react'
import Registration from "../Registration/Registration.jsx"
import NotFound from "../NotFound/NotFound.jsx"
import Login from '../Login/Login.jsx'
import HomeMiddleware from '../Home/HomeMiddleware.jsx'
import { Navigate, Route, Routes } from 'react-router-dom'
import AddPostFormMiddleware from '../PostForm/AddPostFormMiddleware'
import EditPostFormMiddleware from '../PostForm/EditPostFormMiddleware'

export default function App() {


  return (
    <>

      <Routes>
        <Route path="/" element={<Navigate to="/home" />} />
        <Route path="/register" element={<Registration />} />
        <Route path="/login" element={<Login />} />

        <Route path="/home" element={<HomeMiddleware />} />
        <Route path="/addpost" element={<AddPostFormMiddleware />} />
        <Route path="/editpost/:id" element={<EditPostFormMiddleware />} />


        <Route path="*" element={<NotFound />} />
      </Routes>

    </>
  )
}
