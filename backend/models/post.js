const mongoose = require('mongoose');
const { Schema } = mongoose;

const postSchema = new Schema({
  title: {type: String, required: true, trim:true},
  description: {type: String, trim:true, required:false},
  price: {type: Number, required:true},
  createdBy: {type: mongoose.Schema.Types.ObjectId, ref:'User',  required: true}
});

module.exports =  mongoose.model('Post', postSchema);