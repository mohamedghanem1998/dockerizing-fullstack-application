const joi = require('joi')

const rules = joi.object({
    name: joi.string().trim(true).required(),
    age: joi.number().integer().min(0).max(100).allow(null),
    address: joi.string().trim(true).allow(null)
})


module.exports = rules