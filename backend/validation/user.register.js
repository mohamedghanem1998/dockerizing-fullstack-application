const joi = require('joi')

const rules = joi.object({
    name: joi.string().trim(true).required(),
    email: joi.string().email().trim(true).required(),
    password: joi.string().trim(true).required(),
    cpassword: joi.string().trim(true).equal(joi.ref('password')).required(),
    age: joi.number().integer().min(0).max(100).allow(null),
    address: joi.string().trim(true).allow(null)
})


module.exports = rules