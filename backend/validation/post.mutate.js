const joi = require('joi')

const rules = joi.object({
    title: joi.string().trim(true).required(),
    description: joi.string().trim(true).required(),
    price: joi.number().integer().required(),
})


module.exports = rules