const router = require('express').Router()
const {create, destroy, update, index, userPosts, show} = require("../controllers/post.controller");
const auth = require('../middleware/auth')


router.get('/index', auth,index)
router.get('/show/:id', auth,show)

router.get('/user/:createdBy', userPosts)
router.post('/create', auth, create)
router.delete('/delete/:id', auth, destroy)
router.patch('/update/:id', auth, update)


module.exports = router