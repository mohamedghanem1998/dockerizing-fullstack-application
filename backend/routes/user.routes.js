const router = require('express').Router()
const { register, login, destroy, update, index, show } = require("../controllers/user.controller");
const auth = require('../middleware/auth')

router.post('/register', register)
router.post('/login', login)
router.delete('/delete/:id', auth, destroy)
router.patch('/update/:id', auth, update)
router.get('/index', index)
router.get('/show/:id', show)



module.exports = router