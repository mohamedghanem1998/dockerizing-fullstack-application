const userSchema = require('../models/user')
const postSchema = require('../models/post')

const bcrypt = require('bcryptjs')
const dotenv = require('dotenv').config({ path: './config/config.env' });
const jwt = require('jsonwebtoken')
const registrationRules = require('../validation/user.register')
const updatenRules = require('../validation/user.update')



const register = async (req, res) => {

    const { name, age, email, password, cpassword, address } = req.body
    const match = await userSchema.findOne({ email })
    const validation = registrationRules.validate({ name, age, email, password, cpassword, address }, { abortEarly: false })

    if (validation.error || match) {
        let errors = []
        validation.error ? validation.error.message.split(".").forEach(err => errors.push(err)) : ''
        match ? errors.push("Email already in use") : ''

        return res.status(422).json({ errors })
    }


    const securePassword =  await bcrypt.hash(password,  await bcrypt.genSalt(10))
    const newUser = new userSchema({ name, age, email, password: securePassword, address })
    await newUser.save()

    res.status(201).send("Registeration successful")

}


const login = async (req, res) => {
    const { email, password } = req.body
    const match = await userSchema.findOne({ email })

    if (match) {
        if ( await bcrypt.compare(password, match.password)) {

            const token = jwt.sign({ id: match.id, email: match.email, name: match.name }, process.env.JWT_SECRET, { expiresIn: process.env.JWT_EXPIRATION });
            res.status(200).json({ token });
        }

        else {
            res.status(400).json({ error: "Invalid Credentials" });
        }
    }

    else {
        res.status(400).json({ error: "Invalid Credentials" });

    }
}

const destroy = async (req, res) => {
    try {
        const match = await userSchema.findOne({ _id: req.params.id });
        if (match.id != req.user.id) {
            return res.status(403).json({ error: "Unauthorized Request" })
        }
        await postSchema.deleteMany({ createdBy: req.params.id});
        await userSchema.deleteOne({ _id: req.params.id })
        return res.status(200).send("Your Account has been deleted")

    }
    catch (err) {
        res.status(400).json({ error: "No user found" });
    }
}


const update = async (req, res) => {
    try {
        const match = await userSchema.findOne({ _id: req.params.id });

        if (match.id != req.user.id) {
            return res.status(403).json({ error: "Unauthorized Request" })
        }

        else {
            const { name, age, address } = req.body
            const validation = updatenRules.validate({ name, age, address }, { abortEarly: false })

            if (validation.error) {
                let errors = []
                validation.error ? validation.error.message.split(".").forEach(err => errors.push(err)) : ''
                return res.status(422).json({ errors })
            }


            await userSchema.findOneAndUpdate({ _id: req.user.id},{ name, age, address })
            return res.status(200).send("Your Account has been updated")
        }


    }
    catch (err) {
        res.status(400).json({ error: "No user found" });
    }
}


const index = async (req, res) => {
    const users = await userSchema.find()
    res.json({ users })
}


const show = async (req, res) => {

    try {
        const user = await userSchema.findOne({ _id: req.params.id })
        res.json({ user })

    } catch (error) {
        res.status(404).send("User not found")
    }

}

module.exports = { register, login, destroy, update, index, show }