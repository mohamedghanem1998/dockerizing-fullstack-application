const postMutateValidation = require('../validation/post.mutate');
const postSchema = require('../models/post')

const create = async (req, res) => {
    
    const { title, description, price } = req.body
    const validation = postMutateValidation.validate({ title, description, price }, { abortEarly: false })

    if (validation.error) {
        let errors = []
        validation.error ? validation.error.message.split(".").forEach(err => errors.push(err)) : ''
        return res.status(422).json({ errors })
    }

    const newPost = new postSchema({ title, description, price, createdBy: req.user.id })
    await newPost.save()
    res.status(200).send("Post Created!")
}

const destroy = async (req, res) => {

    try {
        const match = await postSchema.findOne({ _id: req.params.id });
        if (match.createdBy != req.user.id) {
            return res.status(403).json({ error: "Unauthorized Request" })
        }
        await postSchema.deleteOne({ _id: req.params.id })
        return res.status(200).send("Your post has been deleted ")

    }
    catch (err) {
        res.status(400).json({ error: "No post found" });
    }
}


const update = async (req, res) => {

    try {
        const match = await postSchema.findOne({ _id: req.params.id });
        if (match.createdBy != req.user.id) {
            return res.status(403).json({ error: "Unauthorized Request" })
        }

        else {
            const { title, description, price } = req.body
            const validation = postMutateValidation.validate({ title, description, price }, { abortEarly: false })

            if (validation.error) {
                let errors = []
                validation.error ? validation.error.message.split(".").forEach(err => errors.push(err)) : ''
                return res.status(422).json({ errors })
            }

            await postSchema.findOneAndUpdate({ _id: req.params.id }, { title, description, price })
            return res.status(200).send("Your post has been updated ")
        }

    }

    catch (error) {
        res.status(400).json({ error });
    }
}

const index = async (req, res) => {

    const posts = await postSchema.find().populate('createdBy', ['name', 'email'])
    res.json({ posts })
}


const show = async (req, res) => {

    const post = await postSchema.findOne({_id:req.params.id})
    res.json({ post })
}


const userPosts = async (req, res) => {

    try {
        const posts = await postSchema.find({ createdBy: req.params.createdBy })
        res.json({ posts })
    }
    catch (error) {
        res.json({ error })

    }


}


module.exports = { create, destroy, update, index, userPosts, show }