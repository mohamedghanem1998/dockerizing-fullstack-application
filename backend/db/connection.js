const mongoose = require('mongoose')

const connect = () => {
    mongoose.connect(`${process.env.MONGODB_URI}`, {serverSelectionTimeoutMS: 100000}).then((result)=>{
        console.log(`MongoDB connection successful at ${process.env.MONGODB_URI}`)
    }).catch(err => console.log(err))
}


module.exports = connect