const jwt = require('jsonwebtoken');
const userSchema = require('../models/user') 
const dotenv = require('dotenv').config({ path: "./config/config.env" });


const protect = async (req, res, next) => {

    let token

    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
        token = req.headers.authorization.split(' ')[1]

        try {
            const decoded = jwt.decode(token, process.env.JWT_SECRET)
            req.user = await userSchema.findOne({_id: decoded.id})
            next()
        }

        catch (error) {
            res.status(400).send("Incorrect token")
        }
    }

    else {
        res.status(400).send("Please send Bearer /your_token/ in the request's Authorization header")

    }

}


module.exports = protect