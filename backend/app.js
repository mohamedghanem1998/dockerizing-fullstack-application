const express = require('express');
const app = express();
// const dotenv = require('dotenv').config({path: './config/config.env'});
const connect = require('./db/connection')
var cors = require('cors')


app.use(express.json());
app.use(cors())

connect()


// Mount Routers
const userRouter = require('./routes/user.routes')
app.use('/users', userRouter)

const postRouter = require('./routes/post.routes')
app.use('/posts', postRouter)




app.get('/', (req, res) => {
    res.send("Backend Server")
})

const PORT = process.env.PORT || 8000;
app.listen(PORT, () => {
    console.log(`Backend server running on port: ${PORT}`)
})