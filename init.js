// Switch to your desired database
const db = db.getSiblingDB('my_db');

// Create a collection and insert some sample data
db.my_collection.insertMany([
    { name: "Alice", age: 25 },
    { name: "Bob", age: 30 },
    // Add more sample documents as needed
]);